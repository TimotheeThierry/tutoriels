Tutoriel pour utilisation de GitLab
===================================

Ce tutoriel a pour objectif de permettre l'utilisation de GitLab via un navigateur web. Il présente les 
principales fonctionnalités avec la navigation dans le projet, le téléchargement du projet, l'ajout d'un 
nouveau fichier, l'édition d'un fichier déjà présent... À noter que GitLab.com a un support du français 
relativement limité.

GitLab
------

GitLab est un projet [open core](https://fr.wikipedia.org/wiki/Open_core). Cela signifie que le cœur du 
projet est [open source](https://fr.wikipedia.org/wiki/Open_source) (le code source du projet est ouvert) 
mais certaines fonctionnalités spécifiques "en plus" sont payantes. 

GitLab est une [forge](https://fr.wikipedia.org/wiki/Forge_(informatique)). C'est-à-dire que GitLab a pour 
objectif de permettre à plusieurs personnes de participer au développement d'un même projet. GitLab.com est 
le service web correspondant.

GitLab.com héberge des projets utilisant [git](https://fr.wikipedia.org/wiki/Git). 

git
---------------

[git](https://fr.wikipedia.org/wiki/Git) est un logiciel de gestion de version. Il va permettre de 
conserver un historique complet des versions d'un projet de façon décentralisée. git est très utilisé par 
la communauté des développeurs informatiques parce  qu'il facilite entre autres le travail collaboratif sur 
un projet avec notamment la gestion de conflits quand deux développeurs travaillent sur un même fichier.

Il est aussi possible de revenir rapidement à différentes versions d'un projet. Une "branch" au sens git 
permet de s'éloigner du projet principal pour développer par exemple une nouvelle fonctionnalité ou 
répondre à une demande. Une fois finie il est possible de revenir au projet principal simplement sans avoir 
à faire de multiple copier/coller ou avoir plusieurs dossiers contenant le même projet ou programme sous 
différentes versions. Les différentes versions cohabitent dans un seul dossier et l'on peut facilement 
passer de l'une à l'autre sans perte d'information.

git offre la notion de "commit" qui peut être traduite en version: à une succession de changements 
effectués sur le projet (suppression de fichiers, ajout de fichiers, modification de fichiers, création de 
dossiers...) depuis le dernier "commit" est associé un message explicatif et son auteur. 

Par exemple suite à l'ajout d'un programme sas on peut associer une succession de messages "commit" du 
type:

> commit 1:
> 
> Ajout du programme macro_age.sas permettant d'obtenir la tranche d'âge des malades "date de naissance" 
> de la table DCIR Simplifié
> 
> commit 2:
> 
> Modification du code du script macro_age.sas suite au changement du nom de la variable age dans le DCIR 
> Simplifié
> 
> commit 3:
> 
> Ajout du script macro_commune.sas pour une meilleure gestion des codes communes



Cette succession de "commit" fournit l'historique d'un projet, ils doivent être clairs et explicites pour 
faciliter la compréhension des modifications successives apportées à un projet et la navigation dans ces 
différentes versions.

Écran d'accueil de GitLab
------------------------

![1_Accueil](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/1_accueil.PNG)

L'écran d'accueil est divisé en plusieurs parties:
- la partie du haut est liée à GitLab.com avec la navigation à travers l'ensemble de vos projets (**1**), 
vos différents groupes (**2**) ou la gestion de votre profil (**3**)
- la partie de gauche est centrée sur le projet courant avec par exemple le wiki (**6**)
- le cœur de la page se concentre sur l'exploration des fichiers, le README et diverses fonctionnalités 
comme le téléchargement (**5**) ou le code permettant de télécharger le projet en ligne de commande (**4**)
- La barre de recherche (**7**) permet d'effectuer une recherche dans le projet à la fois sur les noms de 
fichiers mais aussi dans le contenus de ceux-ci

![3_Acceuil_Fin](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/3_acceuil_fin.PNG)

En faisant défiler vers le bas on voit plus précisément:
- Le dernier commit sur le projet
- Le système de fichiers, cela permet une navigation à travers les dossiers et fichiers présents dans le 
projet
- Le README, il expose rapidement l'objectif du projet. Il peut être présent dans l'ensemble de 
l'arborescence de fichiers pour préciser le rôle de tel ou tel dossier. À noter que le README est en 
[markdown](https://fr.wikipedia.org/wiki/Markdown)

![10_settings](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/10_settings.PNG)

Les paramètres courants de votre utilisateur sont consultables en cliquant sur le bouton en haut à droite 
puis sur "Settings"

Téléchargement du projet
-----------------------

![2_Accueil_Download](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/2_accueil_download.PNG)

Pour télécharger le projet il suffit de cliquer sur le "bouton nuage" et cliquer ensuite sur "Download zip" 
dans le menu déroulant.

Navigation dans le système de fichiers
-----------------------

La navigation se fait aisément en cliquant sur les dossiers ou fichiers.

![4_Système_de_fichier](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/4_système_de_fichier.PNG)

Ajout d'un nouveau fichier
--------------------------

Une fois dans le bon dossier on peut ajouter un fichier en cliquant sur le "+" à la suite de quoi plusieurs 
choix sont possibles

![5_Ajout_d_un_fichier](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/5_ajout_d_un_fichier.PNG)

Il y a trois choix possibles:
- "New file" qui permet la création d'un fichier *via* GitLab.com. L'édition de ce fichier se fera ensuite 
en utilisant l'éditeur de texte dans un navigateur. Cette utilisation est plutôt conseillée dans le cas 
d'un README
- "Upload file" qui permet l'ajout d'un fichier depuis son ordinateur
- "New directory" qui permet la création d'un nouveau dossier

![6_Upload_file](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/6_upload_file.PNG)

Quand l'option "Upload file" est choisie, il est possible de faire glisser le fichier depuis l'explorateur 
de fichier ou simplement cliquer pour le retrouver dans votre système de fichier. 

Il est important d'indiquer un message de commit clair. Celui-ci permet de bien comprendre l'historique du 
projet et le pourquoi de l'ajout/modification d'un fichier.

"Upload file" permet d'exécuter l'opération.

Modification d'un fichier existant
--------------------------
Quand on clique sur un fichier on obtient l'écran suivant:
![7_Modification_d_un_fichier](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/7_modification_d_un_fichier.PNG)
On peut faire plusieurs manipulations depuis cette interface:
- Il est possible de copier l'ensemble du fichier (**1**), équivalent à un ```Ctrl+A Ctrl+C```
- Il est possible d'utiliser l'éditeur de GitLab pour directement modifier le fichier dans son navigateur 
web via le bouton "Edit" (**2**)
- Le "Replace" (**3**) permet de revenir à un écran équivalent de l'"Upload file" précédent. La nouvelle 
version du fichier remplacera le fichier existant
- On peut supprimer le fichier (**4**)

Edition d'un fichier existant
----------------------------
En cliquant sur le bouton "Edit" précédent on se retrouve sur l'écran suivant:

![8_Edition_haut](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/8_edition_haut.PNG)

L'onglet "Write" permet de modifier le fichier. Le "Preview changes" permet de voir le "diff" sur ce 
fichier c'est-à-dire l'interprétation que git a sur les modifications apportées à ce fichier. Dans le cas 
d'un fichier en markdown (.md) comme par exemple README.md le bouton "Preview changes" devient "Preview" et 
permet de voir le résultat des modifications du fichier.

![9_Edition_fin](https://gitlab.com/DREES/tutoriels/raw/master/img/tutoriel/9_edition_fin.PNG)

Une fois l'édition finie on peut indiquer le message de commit approprié, ajouter les changements ou 
annuler la modification.
